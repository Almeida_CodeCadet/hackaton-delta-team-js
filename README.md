**Team Delta Bowie Project - Frontend**

Our e-commerce project Developed in the context of <Academia de Código_> hackathon challenge (24h non-stop programming).

**Developed by:** André Castro, Juliana Liberali, Pedro Gaspar, Hugo Leal, Luís Almeida

This application is prepared to consume the developed backend REST API.
It was design to have a simple an intuitive interface with a Mars related theme

**Technologies**
- Javascript
- HTML/CSS
- Bootstrap for responsiveness
- Google Sign-in
- OAuth2.0
- FetchAPI
- Session Storage

**Current State**
- Google login authentication is implemented
- Add user is implemented

**Next Steps**
- Develop the orders list so they persist on database
- Integrate payment system (MBway/Paypal...)
- Send email to the customer with the order details

