fillUserInfo();
fillProductInfo();

function fillUserInfo() {

        let html = '';
        let sum = 0;

        html += '<tr>';
        html += '<td>';
        html += ' Bowie Delta, Inc.<br> Parque de Exposições de Aveiro<br> Av. Dom Manuel de Almeida Trindade, 3810-164';
        html += '</td>';
        html += '<td>';
        html += '    <br>' + sessionStorage.getItem('userFirstName') + ' ' + sessionStorage.getItem('userLastName') +'<br>' + sessionStorage.getItem('userEmail') ;
        html += '</td>';
        html += ' </tr>';

        document.getElementById("userInfo").innerHTML = html;
}
;


function fillProductInfo() {
    let products = JSON.parse(sessionStorage.getItem('products'));
    if (products != null) {

        let html = '';
        let sum = 0;
        Array.prototype.forEach.call(products, function(el, index, array) {
            
            html += '<tr class="item">';
            html += '<td>' + el.productName + '</td>';
            html += '<td>' + el.productPrice * el.Quantity + '</td>';
            html += '</tr>';

            sum = sum + el.productPrice * el.Quantity;
        });

        document.getElementById('products').innerHTML = html;
    }
};

document.getElementById("totalValue").innerHTML = "Total: " + sessionStorage.getItem("totalValue") + "€";
document.getElementById("totalValue2").innerHTML = "Total: " + sessionStorage.getItem("totalValue") + "€";
