function onSignIn(response) {
          // getting the user information.
          var perfil = response.getBasicProfile();
          var userGoogleID = perfil.getId();
          var userName = perfil.getName();
          var userEmail = perfil.getEmail();
          var userPicture = perfil.getImageUrl();

          //getting first and last name
          var nameArr = userName.replace(/(<[^>]*>)/g,'').split(' ');

          sessionStorage.setItem('userGoogleId', userGoogleID);
          sessionStorage.setItem('userName', userName);
          sessionStorage.setItem('userFirstName', nameArr[0]);
          sessionStorage.setItem('userLastName', nameArr[nameArr.length-1]);
          sessionStorage.setItem('userEmail', userEmail);
          sessionStorage.setItem('userPicture', userPicture);
          sessionStorage.setItem('addressId', "batata");

          //getting googleId from database and validating if the user exists
          fetch('http://localhost:8080/Hackthon/user/googleId/' + userGoogleID)
                .then(response => {
                    return response.json()
                })
                .then(data => {
                    console.log(data);
                    if (data == true){
                    //Redirect para a página do index
                    //console.log("entrei true");
                    window.location.href = "http://localhost:5500/index.html";
                    }else{
                //Redirect para a página do Register
                //console.log("entrei false");
                window.location.href = "http://localhost:5500/register.html";
                    }
                })
                .catch(err => {
                    // Do something for an error here
                })

          // Token
          var LoR = response.getAuthResponse().id_token;
          console.log("Token: " + LoR);

      };