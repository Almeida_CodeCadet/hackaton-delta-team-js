let userGoogleID = sessionStorage.getItem('userGoogleId')

//get User
fetch('http://localhost:8080/Hackthon/user/userGoogleId/' + userGoogleID)
.then(response => {
    return response.json()
})
.then(data => {
        if(data.firstName == undefined){
            return;
        }

        document.getElementById("firstLastName").innerHTML = data.firstName + " " + data.lastName;
        document.getElementById("firstName").value = data.firstName;
        document.getElementById("lastName").value = data.lastName;
        document.getElementById("email").value = data.email;
        document.getElementById("phone").value = data.phone;
        sessionStorage.setItem('userId', data.id);

    }
)
.catch(err => {
    // Do something for an error here
})

//Put User
const putButton = document.getElementById('putUser');
putButton.addEventListener('click', function (e) {
    e.preventDefault();

    //userForm
    const userForm = {
        firstName: document.getElementById("firstName").value,
        lastName: document.getElementById("lastName").value,  
        email: document.getElementById("email").value,
        phone: document.getElementById("phone").value
    };
    putUser(userForm);

    //addressForm
    const insertForm = {
        street: document.getElementById("street").value,
        city: document.getElementById("city").value,
        district: document.getElementById("district").value,  
        country: document.getElementById("country").value,
        zipCode: document.getElementById("zipCode").value
    };
    if(sessionStorage.getItem('addressId') == "batata"){
        alert("entrei")
        sessionStorage.setItem('addressId',1);
        //post adress
        sendAddress(insertForm);
    }else{

    }

    location.reload();
    return false;
})

//Delete User
const deleteButton = document.getElementById('deleteUser');
deleteButton.addEventListener('click', function (e) {
    e.preventDefault();
    fetch('http://localhost:8080/Hackthon/user/delete/' + sessionStorage.getItem('userId'), {
            method: 'DELETE',
          })
          .then(res => res.text()) // or res.json()
          .then(res => console.log(res))
          location.reload();
          return false;
})


//functions
putUser = (body) => {
        
    fetch('http://localhost:8080/Hackthon/user/edit/' + sessionStorage.getItem('userId'), {
        method: 'PUT',
        headers: {
            'Accept' :'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(body)
    }).then((res) => res.json())
    .then(function(data) {
        ('Request succeeded with JSON response', data);
        //Validação        
      })
    .then((body) =>  console.log(body))
    .catch((err)=>console.log(err))    
    };

    putAddress = (body) => {
        
        fetch('http://localhost:8080/Hackthon/address/edit/' + sessionStorage.getItem('addressId'), {
            method: 'PUT',
            headers: {
                'Accept' :'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(body)
        }).then((res) => res.json())
        .then(function(data) {
            ('Request succeeded with JSON response', data);
            //Validação        
          })
        .then((body) =>  console.log(body))
        .catch((err)=>console.log(err))    
        };

    sendAddress = (body) => {
        fetch('http://localhost:8080/Hackthon/address/' + sessionStorage.getItem('userId') + '/create', {
            method: 'POST',
            headers: {
                'Accept' :'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(body)
        }).then((res) => res.json())
        .then(function(data) {
            ('Request succeeded with JSON response', data);
          })
        .then((body) =>  console.log(body))
        .catch((err)=>console.log(err))    
        }