document.getElementById("shoppingCartButon").addEventListener("click", function() {
    addProduct()
});

function addProduct(){
    console.log("entrei");
    var products = null;
    var productId = document.getElementById("productId").innerHTML;
    var productPrice = document.getElementById("productPrice").innerHTML;
    var productName = document.getElementById("productName").innerHTML;
	var objectID = -1;

    if(sessionStorage.getItem('products')){
        products = JSON.parse(sessionStorage.getItem('products')); 
    } else {
		products = new Array();
	}
	
	Array.prototype.forEach.call(products, function(el, index, array) {		
		if (el.productName == productName) {
			objectID = index;
			products[index].Quantity += 1;
		}
	});

    if (objectID == -1 || products.length == 0) {	
		products.push({"productId": productId ,"productPrice": productPrice,"productName":productName, "Quantity":1});
	}

    sessionStorage.setItem('products', JSON.stringify(products));
    location.reload();
}

