const form = document.getElementById('userSubmit');
form.addEventListener('submit', function (e) {
e.preventDefault();

const insertForm = {
    googleId: sessionStorage.getItem("userGoogleId"),
    firstName: document.getElementById("userFirstName").value,
    lastName: document.getElementById("userLastName").value,  
    email: document.getElementById("userEmail").value,
    phone: document.getElementById("userPhone").value
};

sendUser = (body) => {
    fetch('http://localhost:8080/Hackthon/user/create', {
        method: 'POST',
        headers: {
            'Accept' :'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(body)
    }).then((res) => res.json())
    .then(function(data) {
        ('Request succeeded with JSON response', data);
      })
    .then((body) =>  console.log(body))
    .catch((err)=>console.log(err))    
    }
    console.log(insertForm);
    sendUser(insertForm);
    
    window.location.href = "http://localhost:5500/index.html";
});

