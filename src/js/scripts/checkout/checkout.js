document.getElementById("cart").addEventListener("click", function() {
    checkout();
});

function checkout() {
    let products = JSON.parse(sessionStorage.getItem('products'));
    if (products.length > 0) {

        let html = '';
        let sum = 0;
        Array.prototype.forEach.call(products, function(el, index, array) {


            html += '<tr>';
            html += '<td>' + el.productName + '</td>';
            html += '<td>' + el.productPrice + '</td>';
            html += '<td class="qty"><input type="text" class="form-control" id="input1" value="' + el.Quantity + '"></td>';
            html += '<td>' + el.productPrice * el.Quantity + '</td>';
            html += '<td>';
            html += '<a href="#" class="btn btn-danger btn-sm">';
            html += '<i class="fa fa-times"></i>';
            html += '</a>';
            html += '</td>';
            html += '</tr>';

            sum = sum + el.productPrice * el.Quantity;
        });

        sessionStorage.setItem("totalValue", sum);
        document.getElementById('total').innerHTML = sum;
        document.getElementById('checkoutBody').innerHTML = html;
    }
};